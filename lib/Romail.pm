#!/usr/bin/env perl
package Romail;
use v5.24;
use warnings;
use experimental 'signatures';

use App::Easer::V2 -command => -spec => {
   help => 'check email stuff',
   description => <<'END',

Check email stuff

END
};

sub execute ($self) {
   say 'check email stuff';
   say ref($self);
   say for our @ISA;
   return 0;
}

1;
