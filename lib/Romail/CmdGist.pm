#!/usr/bin/env perl
package Romail::CmdGist;
use v5.24;
use warnings;

use App::Easer::V2 -command => -spec => {
   help => 'print the gist of the email',
   aliases => [qw< gist skeleton >],
   description => <<'END',

   <pre> recipients -i file.eml
   <pre> recipients --to -i file.eml
   <pre> recipients --cc -i file.eml

END
   options => [
      {
         getopt => 'input|i=s@',
         help => 'input email file',
      },
      {
         getopt => 'output|o=s',
         help => 'output file',
         default => '-',
      },
      {
         getopt => 'plain|p',
         help => 'plain output, no JSON',
      },
   ],
};

use Moo;
use experimental 'signatures';
use namespace::clean;

with 'Romail::Role::Input', 'Romail::Role::Output';


sub execute ($self) {
   my $plain = $self->config('plain');
   my %collected;
   $self->iterate_emails(
      sub ($input, $email) {
         my $output = '';
         open my $fh, '>:encoding(UTF-8)', \$output;
         $email->dump_skeleton($fh);
         close $fh;
         if ($plain) {
            say $input;
            say $output =~ s{^}{   }rgmxs;
         }
         else {
            $collected{$input} = $output;
         }
      }
   );

   $self->output(\%collected) unless $plain;
   return 0;
}

1;
