#!/usr/bin/env perl
package Romail::Role::Output;
use v5.24;
use warnings;

use Moo::Role;
use experimental 'signatures';

use English;
use JSON::PP ();
use Ouch;

use namespace::clean;

sub __encode_json ($data) {
   state $encoder = JSON::PP->new->ascii->canonical->pretty;
   return $encoder->encode($data);
}

sub raw_stdout ($self) { binmode STDOUT, ':raw'; return \*STDOUT }

sub output ($self, $data) {
   my $dst = $self->config('output') // '-';
   $dst = $dst eq '-' ? $self->raw_stdout : path($dst)->openw_raw;
   $data = __encode_json($data) if ref($data);
   print {$dst} $data or ouch 500, "print(): $OS_ERROR";
   return;
}

1;
