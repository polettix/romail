#!/usr/bin/env perl
package Romail::Role::Input;
use v5.24;
use warnings;

use Moo::Role;
use experimental 'signatures';

use MIME::Parser;
use Path::Tiny;

use namespace::clean;

has _inputs => (is => 'lazy', clearer => 1);
has parser  => (is => 'lazy');

sub _build__inputs($self) { [($self->config('input') // [ '-' ])->@*] }

sub _build_parser ($self) {
   my $parser = MIME::Parser->new;
   $parser->output_to_core(1);
   return $parser;
}

sub iterate_emails ($self, $cb) {
   $self->reset_inputs;
   while (my ($src, $email) = $self->next_email) {
      $cb->($src, $email);
   }
   return $self;
}

sub next_input ($self) { return shift $self->_inputs->@* }

sub next_email ($self) {
   my $input = $self->next_input // return;
   my $email = $self->parse($input eq '-' ? $self->raw_stdin : $input);
   return ($input, $email) if wantarray;
   return $email;
}

sub parse ($self, $src) {
   my $parser = $self->parser;
   return $parser->parse($src) if ref($src);
   return $parser->parse_data($src) unless -e $src;
   return $parser->parse(path($src)->openr_raw);
} ## end sub parse

sub raw_stdin ($self) { binmode STDIN, ':raw'; return \*STDIN }

sub reset_inputs ($self) { $self->_clear_inputs }

1;
