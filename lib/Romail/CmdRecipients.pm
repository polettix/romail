#!/usr/bin/env perl
package Romail::CmdRecipients;
use v5.24;
use warnings;

use App::Easer::V2 -command => -spec => {
   help => 'get recipients',
   aliases => [qw< recipients >],
   description => <<'END',

   <pre> recipients -i file.eml
   <pre> recipients --to -i file.eml
   <pre> recipients --cc -i file.eml

END
   options => [
      {
         getopt => 'address|a!',
         help => 'get addresses only (no display name, etc.)',
      },
      {
         getopt => 'cc',
         help => 'get "cc" recipeints',
      },
      {
         getopt => 'flat|mix|F',
         help => 'mix recipients in a single output array',
      },
      {
         getopt => 'input|i=s@',
         help => 'input email file',
      },
      {
         getopt => 'output|o=s',
         help => 'output file',
         default => '-',
      },
      {
         getopt => 'to',
         help => 'get "to" recipients',
      },
   ],
};

use Moo;
use experimental 'signatures';
use Email::Address::XS 'parse_email_addresses';
use namespace::clean;

with 'Romail::Role::Input', 'Romail::Role::Output';


sub execute ($self) {
   my @wants = grep { $self->config($_) } qw< cc to >;
   @wants = qw< cc to > unless @wants;
   my $flat = $self->config('flat');

   my (%collected, @all);
   $self->iterate_emails(
      sub ($input, $email) {
         my $c = $collected{$input} = $self->recipients($email);
         push @all, map { $c->{$_}->@* } qw< to cc > if $flat;
      }
   );

   $self->output($flat ? \@all : \%collected);
   return 0;
}

sub recipients ($self, $email) {
   my $hdrs = $email->head;
   my $ao = $self->config('address');
   return {
      map {
         my @values =
            map { $ao ? $_->address : $_->as_string }
            map { parse_email_addresses($_)         }
            $hdrs->get_all($_);
         $_ => \@values;
      } qw< to cc >
   };
}

1;
